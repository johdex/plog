﻿using ploggy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace plog_gui
{
    public partial class MissionStatsForm : Form
    {
        List<BriefPlayerStats> statList = null;
        CoalitionBind[] countries = null;

        public MissionStatsForm()
        {
            InitializeComponent();
            
        }

        private Color CoalitionStyle(Coalition coalition, bool highlight = false)
        {
            switch (coalition)
            {
                case Coalition.Neutral:
                    return highlight ? Color.Black : Color.Black;

                case Coalition.Allies:
                    return highlight ? Color.Red : Color.DarkRed;

                case Coalition.Axis:
                    return highlight ? Color.Blue : Color.DarkBlue;
            }

            return Color.Black;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            ShowResults();
        }

        public void ShowResults()
        {
            statsGrid.DataSource = statList;

           /* int count = statList.Count;

            for (int i = 0; i < count; ++i)
            {
                BriefPlayerStats bps = statList[i];
                Coalition coalition = Utility.GetCountryCoalition(countries, bps.GetCountry());
            }*/
        }

        public void SetData(MissionLog missionLog)
        {
            countries = missionLog.MissionCountries;
            List<PlayerStatsInfo> list = missionLog.GetAllPlayerStats();
            statList = new List<BriefPlayerStats>(list.Count);

            foreach (PlayerStatsInfo psi in list)
                statList.Add(new BriefPlayerStats(psi));
        }

        private void statsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            
            if (statList == null)
                return;

            if (e.RowIndex >= statList.Count)
                return;

            BriefPlayerStats bps = statList[e.RowIndex];
            Coalition coalition = Utility.GetCountryCoalition(countries, bps.GetCountry());

            e.CellStyle.BackColor = CoalitionStyle(coalition);
            e.FormattingApplied = true;
        }
    }
}
