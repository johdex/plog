﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ploggy
{
    public class ExportOptions
    {
        public bool ExportMissionFlow = true;
        public bool ExportDeadStats = true;
        public bool ExportMissionStats = true;
        public bool ExportAllPlayers = false;
        public bool ExportMissionInfo = true;
        public bool ExportMissionObjectives = false;
        public bool ExportInteresting = false;
    }
}
